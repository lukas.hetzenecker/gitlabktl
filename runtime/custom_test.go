package runtime

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

func TestCustomRuntimeBuild(t *testing.T) {
	details := Details{
		FunctionName:    "my func",
		FunctionRuntime: "some/address",
		CodeDirectory:   "my/function/",
	}

	client := new(MockClient)
	defer client.AssertExpectations(t)

	client.On("ReadFile", "Dockerfile.template").
		Return("FROM scratch '{{.FunctionName}}'", nil).Once()
	client.On("WriteFiles", "my/function/").Return(nil).Once()

	newRepositoryClient = func(l Location) (Client, error) { return client, nil }

	fs.WithTestFs(func() {
		runtime := CustomRuntime{Details: details}
		err := runtime.Build(context.Background())
		require.NoError(t, err)

		dockerfile, err := fs.ReadFile("my/function/Dockerfile.my-func")
		require.NoError(t, err)

		assert.Equal(t, "FROM scratch 'my func'", string(dockerfile))
	})
}

func TestCustomRuntimeBuildDryRun(t *testing.T) {
	runtime := CustomRuntime{Details{
		FunctionName:    "my func",
		FunctionRuntime: "my/runtime/address",
		CodeDirectory:   "my/function/",
	}}

	summary, err := runtime.BuildDryRun()

	assert.NoError(t, err)
	assert.Equal(t, "preparing a custom runtime", summary)
}

func TestCustomRuntimeSlug(t *testing.T) {
	runtime := CustomRuntime{Details: Details{FunctionName: "my-ąłę-func 1"}}

	assert.Equal(t, "my-ale-func-1", runtime.Slug())
}

func TestCustomRuntimeDockerfilePath(t *testing.T) {
	runtime := CustomRuntime{Details: Details{
		FunctionRuntime: "gitlab.com/some/runtime",
		FunctionName:    "my func ąłę",
	}}
	assert.Equal(t, "Dockerfile.my-func-ale", runtime.DockerfilePath())

	runtime = CustomRuntime{Details: Details{
		FunctionRuntime: "gitlab.com/some/runtime",
		FunctionName:    "my func",
		CodeDirectory:   "echo/",
	}}
	assert.Equal(t, "echo/Dockerfile.my-func", runtime.DockerfilePath())
}
