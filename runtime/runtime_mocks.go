package runtime

import (
	"context"

	"github.com/stretchr/testify/mock"
)

type MockRuntime struct {
	mock.Mock
}

func (runtime *MockRuntime) Build(ctx context.Context) error {
	args := runtime.Called(ctx)

	return args.Error(0)
}

func (runtime *MockRuntime) BuildDryRun() (string, error) {
	args := runtime.Called()

	return args.String(0), args.Error(1)
}

func (runtime *MockRuntime) Directory() string {
	args := runtime.Called()

	return args.String(0)
}

func (runtime *MockRuntime) Image() string {
	args := runtime.Called()

	return args.String(0)
}

func (runtime *MockRuntime) DockerfilePath() string {
	args := runtime.Called()

	return args.String(0)
}
