package runtime

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestReadFile(t *testing.T) {
	client := new(MockClient)
	client.On("ReadFile", "template").Return("FROM scratch", nil).Once()
	defer client.AssertExpectations(t)

	location := Location{
		Address:   "https://gitlab.com/grzesiek/some-runtime",
		Reference: "v0.1alpha1",
	}

	repository := Repository{
		Client:   client,
		Location: location,
	}

	contents, err := repository.ReadFile("template")
	require.NoError(t, err)

	assert.Equal(t, "FROM scratch", contents)
}
