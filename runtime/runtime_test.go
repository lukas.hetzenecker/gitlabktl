package runtime

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	t.Run("short syntax with gitlab prefix", func(t *testing.T) {
		runtime := New(Details{FunctionRuntime: "gitlab/runtimes/ruby"})

		assert.IsType(t, CustomRuntime{}, runtime)
		assert.Equal(t,
			"https://gitlab.com/gitlab-org/serverless/runtimes/ruby",
			runtime.(CustomRuntime).FunctionRuntime)
	})

	t.Run("short syntax with openfaas prefix", func(t *testing.T) {
		runtime := New(Details{FunctionRuntime: "openfaas/classic/ruby"})
		assert.IsType(t, OpenfaasRuntime{}, runtime)
		assert.Equal(t, "openfaas/classic/ruby", runtime.(OpenfaasRuntime).FunctionRuntime)
	})

	t.Run("when runtime is specified as a URL", func(t *testing.T) {
		runtime := New(Details{FunctionRuntime: "https://gitlab.com/gitlab-org/serverless/runtimes/ruby"})

		assert.IsType(t, CustomRuntime{}, runtime)
		assert.Equal(t,
			"https://gitlab.com/gitlab-org/serverless/runtimes/ruby",
			runtime.(CustomRuntime).FunctionRuntime)
	})

	t.Run("when runtime is not specified", func(t *testing.T) {
		assert.IsType(t, DockerfileRuntime{}, New(Details{FunctionName: "my-function"}))
	})
}

func TestImage(t *testing.T) {
	t.Run("when details.ResultingImage is present", func(t *testing.T) {
		runtime := New(Details{ResultingImage: "serverless-function-image", FunctionName: "serverless-function"})
		assert.Equal(t, "serverless-function-image", runtime.Image())
	})

	t.Run("when details.ResultingImage is blank", func(t *testing.T) {
		runtime := New(Details{FunctionName: "serverless-function"})
		assert.Equal(t, "serverless-function", runtime.Image())
	})
}
