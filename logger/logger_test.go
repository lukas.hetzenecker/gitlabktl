package logger

import (
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestDebug(t *testing.T) {
	output := WithTestLogger(func(logger *Logger) {
		logger.SetLevel(logrus.DebugLevel)
		logger.Debug("Test")
	})
	assert.Contains(t, output.String(), "Test")
}

func TestDebugln(t *testing.T) {
	output := WithTestLogger(func(logger *Logger) {
		logger.SetLevel(logrus.DebugLevel)
		logger.Debugln("Test")
	})
	assert.Contains(t, output.String(), "Test")
}

func TestInfo(t *testing.T) {
	output := WithTestLogger(func(logger *Logger) {
		logger.SetLevel(logrus.InfoLevel)
		logger.Info("Test")
	})
	assert.Contains(t, output.String(), "Test")
}

func TestInfof(t *testing.T) {
	output := WithTestLogger(func(logger *Logger) {
		logger.SetLevel(logrus.InfoLevel)
		logger.Infof("Test %s", "format string")
	})
	assert.Contains(t, output.String(), "Test format string")
}

func TestWarn(t *testing.T) {
	output := WithTestLogger(func(logger *Logger) {
		logger.SetLevel(logrus.WarnLevel)
		logger.Warn("Test")
	})
	assert.Contains(t, output.String(), "Test")
}

func TestWarnf(t *testing.T) {
	output := WithTestLogger(func(logger *Logger) {
		logger.SetLevel(logrus.WarnLevel)
		logger.Warnf("%s\n", "Test")
	})
	assert.Contains(t, output.String(), "Test")
}
