package logger

import (
	"bytes"

	"github.com/sirupsen/logrus"
)

// Logger embedded type
type Logger struct {
	*logrus.Logger
}

var logger = Logger{logrus.New()}

// SetLevel exported method
func SetLevel(level logrus.Level) {
	logger.SetLevel(level)
}

// Debug exported method
func Debug(args ...interface{}) {
	logger.Debug(args...)
}

// Debugln exported method
func Debugln(args ...interface{}) {
	logger.Debugln(args...)
}

// Fatal exported method
func Fatal(args ...interface{}) {
	logger.Fatal(args...)
}

// Info exported method
func Info(args ...interface{}) {
	logger.Info(args...)
}

// Infof exported method
func Infof(format string, args ...interface{}) {
	logger.Infof(format, args...)
}

// Warn exported method
func Warn(args ...interface{}) {
	logger.Warn(args...)
}

// Warnf exported method
func Warnf(format string, args ...interface{}) {
	logger.Warnf(format, args...)
}

// WithError exported method
func WithError(err error) *logrus.Entry {
	return logger.WithError(err)
}

// WithField exported method
func WithField(key string, value interface{}) *logrus.Entry {
	return logger.WithField(key, value)
}

// WithTestLogger exported method
func WithTestLogger(block func(logger *Logger)) *bytes.Buffer {
	out := logger.Out
	buffer := new(bytes.Buffer)
	logger.SetOutput(buffer)
	defer logger.SetOutput(out)

	block(&logger)

	return buffer
}
