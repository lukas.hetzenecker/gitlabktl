package serverless

import (
	"gitlab.com/gitlab-org/gitlabktl/runtime"
)

type Function struct {
	Name        string // Function name
	Service     string // Function service name
	Directory   string // Function base directory
	Handler     string
	Image       string // Source or destination image
	Runtime     string // Function runtime address
	Description string
	Labels      []string
	Secrets     []string
	Envs        map[string]string
	Annotations map[string]string
}

func (function Function) ToRuntime() runtime.Runtime {
	details := runtime.Details{
		FunctionName:    function.Name,
		FunctionHandler: function.Handler,
		ResultingImage:  function.Image,
		FunctionRuntime: function.Runtime,
		CodeDirectory:   function.Directory,
	}

	return runtime.New(details)
}
