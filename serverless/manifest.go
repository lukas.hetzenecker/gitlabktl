package serverless

import (
	"errors"
	"fmt"
	"os"
	"path"

	yaml "gopkg.in/yaml.v2"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/logger"
	"gitlab.com/gitlab-org/gitlabktl/registry"
)

// Default serverless configuration file
const ServerlessFile = "serverless.yml"

var providers = []string{"gitlab/knative", "triggermesh"}

// Manifest represents serverless file format
type Manifest struct {
	Definition
}

type Definition struct {
	Service     string
	Description string
	Provider    struct {
		Name        string // see `providers` variable for supported providers
		Registry    string
		Envs        map[string]string
		Environment map[string]string // legacy entry
		Secrets     []string
		EnvSecrets  []string `yaml:"env-secrets"` // legacy entry
		Annotations map[string]string
	}
	Functions map[string]struct {
		Handler     string
		Source      string
		Runtime     string
		Description string
		Labels      []string
		Envs        map[string]string
		Environment map[string]string // legacy entry
		Secrets     []string
		EnvSecrets  []string `yaml:"env-secrets"` // legacy entry
		Annotations map[string]string
	}
}

// Returns a current working directory that is a config base
func (manifest Manifest) BaseDirectory() string {
	pwd, err := os.Getwd()
	if err != nil {
		logger.WithError(err).Fatal("could not determine current working directory")
	}

	return pwd
}

// ToFunctions returns definitions of functions from the serverless
// configuration
func (manifest Manifest) ToFunctions() (functions []Function) {
	baseDirectory := manifest.BaseDirectory()

	for name, function := range manifest.Functions {
		service := fmt.Sprintf("%s-%s", manifest.Service, name)

		newFunction := Function{
			Name:        name,
			Service:     service,
			Handler:     function.Handler,
			Runtime:     function.Runtime,
			Directory:   path.Join(baseDirectory, function.Source),
			Description: function.Description,
			Labels:      function.Labels,
			Envs:        manifest.functionEnvs(name),
			Secrets:     manifest.functionSecrets(name),
			Annotations: manifest.functionAnnotations(name),
		}

		if len(manifest.Provider.Registry) > 0 {
			newFunction.Image = path.Join(manifest.Provider.Registry, service)
		}

		if len(newFunction.Description) == 0 {
			newFunction.Description = manifest.Description
		}

		functions = append(functions, newFunction)
	}

	return functions
}

func (manifest Manifest) functionSecrets(name string) []string {
	// This also handles legacy `env-secrets` and merges top-level definition of
	// secrets with function-level secrets

	function := manifest.Functions[name]

	legacySecrets := append(manifest.Provider.EnvSecrets, function.EnvSecrets...)
	secrets := append(manifest.Provider.Secrets, function.Secrets...)

	if len(legacySecrets) > 0 {
		logger.Warn("legacy `env-secrets:` config found, please use `secrets:`")
	}

	return append(legacySecrets, secrets...)
}

func (manifest Manifest) functionEnvs(name string) map[string]string {
	// This also handles legacy `environment` config and merges top-level
	// definition of environment variables with function-level environment
	// variables

	function := manifest.Functions[name]
	envs := make(map[string]string)

	for env, value := range manifest.Provider.Envs {
		envs[env] = value
	}

	for env, value := range manifest.Provider.Environment {
		logger.Warn("legacy manifest `environment:` config found, please use `envs:`")
		envs[env] = value
	}

	for env, value := range function.Envs {
		envs[env] = value
	}

	for env, value := range function.Environment {
		logger.Warn("legacy function `environment:` config found, please use `envs:`")
		envs[env] = value
	}

	return envs
}

func (manifest Manifest) functionAnnotations(name string) map[string]string {
	function := manifest.Functions[name]
	annotations := make(map[string]string)

	for key, value := range manifest.Provider.Annotations {
		annotations[key] = value
	}

	for key, value := range function.Annotations {
		annotations[key] = value
	}

	return annotations
}

func (manifest Manifest) Validate() error {
	if len(manifest.Service) == 0 {
		return errors.New("service name can't be empty")
	}

	if ok, err := manifest.isProviderSupported(); !ok {
		return fmt.Errorf("invalid provider: %w", err)
	}

	if len(manifest.Functions) == 0 {
		return errors.New("config does not have functions defined")
	}

	return nil
}

func (manifest Manifest) isProviderSupported() (bool, error) {
	if len(manifest.Provider.Name) == 0 {
		return false, errors.New("provider name can not be empty")
	}

	for _, provider := range providers {
		if provider == manifest.Provider.Name {
			return true, nil
		}
	}

	return false, fmt.Errorf("`%s` provider is not supported", manifest.Provider.Name)
}

// NewServerlessConfig returns a new serverless config struct
func NewServerlessConfig() (Manifest, error) {
	return NewServerlessConfigYAML(ServerlessFile)
}

// NewServerlessConfigYAML parses a serverless config file
func NewServerlessConfigYAML(path string) (Manifest, error) {
	var definition Definition

	exists, err := fs.Exists(path)
	if !exists || err != nil {
		return Manifest{}, errors.New("could not find serverless manifest file")
	}

	data, err := fs.ReadFile(path)
	if err != nil {
		return Manifest{}, err
	}

	err = yaml.UnmarshalStrict(data, &definition)
	if err != nil {
		return Manifest{}, err
	}

	manifest := Manifest{Definition: definition}

	if len(manifest.Provider.Registry) == 0 {
		manifest.Provider.Registry = registry.DefaultRepository()
	}

	return manifest, manifest.Validate()
}
