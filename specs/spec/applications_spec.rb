require 'spec_helper'

RSpec.describe 'Serverless application deployment' do
  describe 'deployed services' do
    let(:service) do
      knative_services.find do |service|
        service.name == 'gitlabktl-tests-app-hello'
      end
    end

    it 'deploys a one serverless app' do
      functions = knative_services.select do |service|
        service.name.start_with?('gitlabktl-tests-app')
      end

      expect(functions).to be_one
    end
  end

  describe 'deployed apps' do
    let(:application) do
      Serverless::Application.find('gitlabktl-tests-app-hello')
    end

    it 'has a URL assigned' do
      expect(application).to have_url
    end

    it 'responds with a valid payload' do
      application.post! do |response|
        expect(response.body).to include 'Hello World!'
        expect(response.body).to include application.sha
      end
    end
  end
end
